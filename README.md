# pms-botao-para-topo

Componente botão para redirecionamento ao topo da página para aplicações Vue.

## Instalação

```bash
npm install git+https://dev.santos.sp.gov.br/publico/pms-botao-para-topo.git#VERSAO --save
```

## Utilização

Para utilização do componente, simplesmente importe-o, registre-o e, por fim, inclua-o ao seu template:<br>

**Template:**<br>
```html
<pms-botao-para-topo />
```

**Importação:**<br>
```javascript
import PmsBotaoParaTopo from '@pms/pms-botao-para-topo'
```

**Registro do Componente:**<br>
```javascript
components { PmsBotaoParaTopo }
```