import vue from 'rollup-plugin-vue'
import css from 'rollup-plugin-css-only'
import buble from 'rollup-plugin-buble'
import commonjs from 'rollup-plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import pkg from './package.json'

const componente = 'pms-botao-para-topo'
const input = `./src/${componente}.vue`

const build = (input, output, minificado = false) => {
  output = {
    file: output,
    format: 'es'
  }
  
  const plugins = [
    commonjs(),
    vue(),
    css(),
    buble(),
    minificado ? terser() : null
  ]
  
  return ({ input, output, plugins })
}

export default [
  build(input, pkg.main, true),
  build(input, `./dist/${componente}.esm.js`),
]