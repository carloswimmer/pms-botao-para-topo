import { shallowMount } from '@vue/test-utils'
import PmsBotaoParaTopo from '../../src/pms-botao-para-topo.vue'

describe('pms-botao-para-topo.vue', () => {
  it('está contido em uma div.go-to-top', () => {
    const wrapper = shallowMount(PmsBotaoParaTopo, null)
    expect(wrapper.find('.go-to-top').exists()).toBe(true)
  })
  
  // it('altera o scroll vertical para 0 quando clicado')
  // ------------
  // Verificação do click do botão foi impossibilitada
  // devido ao fato de que o `window.scrollTo` não é im-
  // plementado no JSDOM. Buscar por um contorno a este
  // problema visando a inclusão futura deste teste.

  it('não deve ser exibido quando scrollY for menor que 200', () => mudaScroll(false))
  it('é exibido quando o scrollY for maior do que 200', () => mudaScroll())
  
  const mudaScroll = (rolou = true) => {
    let scrollY   = null
    const wrapper = shallowMount(PmsBotaoParaTopo, { attachToDocument: true })
    
    window.scrollY = scrollY = rolou ? 201 : 0
    window.dispatchEvent(new CustomEvent('scroll', { detail: scrollY }))
    
    const verificacao = rolou ? (window.scrollY > 200) : (window.scrollY < 200)
    
    expect(verificacao).toEqual(true)
    expect(window.scrollY).toEqual(scrollY)
    expect(wrapper.vm.rolouTela).toEqual(rolou)
    expect(wrapper.find('.go-to-top').isVisible()).toEqual(rolou)
  }
})