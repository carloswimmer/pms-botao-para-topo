module.exports = {
    extends: [
        'plugin:vue/strongly-recommended'
    ],
    rules: {
        'vue/no-unused-vars': 'error'
    }
}